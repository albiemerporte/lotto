
from PyQt6.uic import loadUi
from PyQt6.QtWidgets import QApplication
from sqlshot import sqlqueryselecttbl, sqlquerysaverecord, sqlqueryidsearch, sqlquerydeleterec


class Lotto:
    def __init__(self):
        self.lottoui = loadUi('lottoui.ui')
        self.lottoui.show()
        self.lottoui.pbInsert.clicked.connect(self.getcombodata)
        self.lottoui.listHistory.itemPressed.connect(self.displayinfo)
        self.lottoui.pbDelete.clicked.connect(self.deletelistdata)
        
        if self.lottoui.listHistory.count() == 0:
            self.readdata()
    
    # delete list data but directly delete from database then update the listwidget
    def deletelistdata(self):
        getlistdata = self.lottoui.listHistory.selectedItems()
        for items in getlistdata:
            item = items.text()
        try:
            todelete = self.removetup(item)
            #print(todelete)
            sqlquerydeleterec(todelete)
            self.readdata()
            self.lottoui.lblMsg.setText("Success Deleted")
        except NameError:
            self.lottoui.lblMsg.setText("Please Select Items From List To Delete")
        
    @staticmethod
    def removetup(item):
        splited = item.split(".")
        cleanreturn = splited[0].strip()
        return cleanreturn
    
    # to display info of list data when pressed
    def displayinfo(self, item):
        item = (item.data(0))
        print(item)
        numitem = self.removetup(item)
        
        self.lottoui.lblDate.setText(str(sqlqueryidsearch(numitem)[1]) + "-" +\
                                         str(sqlqueryidsearch(numitem)[2]) + "-" +\
                                         str(sqlqueryidsearch(numitem)[3]))
        self.lottoui.lblJackpot.setText(str(sqlqueryidsearch(numitem)[10]))
        self.lottoui.lblWinner.setText(str(sqlqueryidsearch(numitem)[11]))
        self.lottoui.lblNumId.setText(str(sqlqueryidsearch(numitem)[0]))
        
        #print(result)
     
    def readdata(self):
        self.lottoui.listHistory.clear()
        pall = reversed(sqlqueryselecttbl())
        
        try:
            for tup in pall:
                lottodata = (f"{tup[0]}.     {tup[4]} {tup[5]} {tup[6]} {tup[7]} {tup[8]} {tup[9]}")    
                self.lottoui.listHistory.addItem(lottodata)
        except NameError:
            self.lottoui.lblMsg.setText("Empty Database")
            self.lottoui.listHistory.addItem("")
    
    def combosettext(self):        #non data function
        self.lottoui.cmbNum1.setCurrentText("SELECT")
        self.lottoui.cmbNum2.setCurrentText("SELECT")
        self.lottoui.cmbNum3.setCurrentText("SELECT")
        self.lottoui.cmbNum4.setCurrentText("SELECT")
        self.lottoui.cmbNum5.setCurrentText("SELECT")
        self.lottoui.cmbNum6.setCurrentText("SELECT")
    
    def getcombodata(self):
        num1 = self.lottoui.cmbNum1.currentText()
        num2 = self.lottoui.cmbNum2.currentText()
        num3 = self.lottoui.cmbNum3.currentText()
        num4 = self.lottoui.cmbNum4.currentText()
        num5 = self.lottoui.cmbNum5.currentText()
        num6 = self.lottoui.cmbNum6.currentText()
        
        if num1 == 'SELECT' or num2 == 'SELECT' or num3 == 'SELECT' or \
           num4 == 'SELECT' or num5 == 'SELECT' or num6 == 'SELECT':
            self.lottoui.lblMsg.setText("Please Select Number Of Every Each ComboBox")
        else:
            newinsert = (int(num1), int(num2), int(num3), int(num4), int(num5), int(num6))
            #print(newinsert)
            sqlquerysaverecord(newinsert)
            self.combosettext()   #method
            self.readdata()       #method

if __name__ == '__main__':
    app = QApplication([])
    main = Lotto()
    app.exec()